# Акварельный мир
Любое фото из видео становится частью акварельного мира.

## Демо
[Сделать фото](https://lutretyakova.gitlab.io/projects/react-watercolor-world/)

## Блог
[Блог Акварельного мира](https://lutretyakova.gitlab.io/react-watercolor-world.html)