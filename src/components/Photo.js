import React, { Component } from 'react';

import defaultCameraIcon from '../images/default_camera.svg';

export default class Photo extends Component {
    render() {
        const imgUrl = this.props.imgUrl !== null ? this.props.imgUrl : defaultCameraIcon;

        const divStyle = {
            backgroungImage: imgUrl !== null ? imgUrl : '',
            width: `${this.props.width}px`,
            height: `${this.props.height}px`
        }

        let imgStyle = {};
        if (imgUrl !== defaultCameraIcon) {
            this.changeImgBackground(imgUrl);
        } else {
            imgStyle = {
                width: divStyle.width,
                height: divStyle.height,
            }
        }

        return (
            <div className="watercolor" id="watercolor" style={divStyle}>
                <img src={imgUrl} style={imgStyle} alt='' id="watercolor-img"/>
            </div>
        )
    }

    changeImgBackground(imgUrl) {
        const before = '.watercolor::before';
        const after = '.watercolor::after';
        const sheets = document.styleSheets;
        for (let sheet of sheets) {
            for (let rule of sheet.cssRules) {
                if (rule.type === CSSRule.SUPPORTS_RULE) {
                    for (let innerRule of rule.cssRules) {
                        if (innerRule.selectorText === before) {
                            innerRule.style['background-image'] = `url(${imgUrl}), url(${imgUrl})`
                        }
                        if (innerRule.selectorText === after) {
                            innerRule.style['background-image'] = `url(${imgUrl})`
                        }
                    }
                }
            }
        }
    }


}