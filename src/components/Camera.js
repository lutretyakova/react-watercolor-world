import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';

import Photo from './Photo';

class Camera extends Component {
    constructor(props) {
        super(props);
        this.state = {
            streaming: false,
            width: this.props.cameraWidth,
            imgUrl: null
        }
    }

    componentDidMount() {
        this.video = document.getElementById('video');
        this.canvas = document.getElementById('canvas');
        navigator.mediaDevices.getUserMedia({ video: true, audio: false }).then(
            (stream) => {
                this.handleVideo(stream);
            }
        ).catch((err) => {
            alert(err)
        });
    }

    handleVideo(stream) {
        this.video.srcObject = stream;
        this.video.play();
    }

    handleCanPlay(e) {
        if (!this.state.streaming) {
            const height = this.video.videoHeight / (this.video.videoWidth / this.state.width);

            this.video.setAttribute('width', this.state.width);
            this.video.setAttribute('height', height);
            this.canvas.setAttribute('width', this.state.width);
            this.canvas.setAttribute('height', height);

            this.setState({
                streaming: true,
                height: height,
            })
        }
    }

    handleClick() {
        if (this.state.width && this.state.height) {
            const context = this.canvas.getContext('2d');
            context.drawImage(this.video, 0, 0, this.state.width, this.state.height);
            const imgUrl = this.canvas.toDataURL('image/png');
            this.setState({
                imgUrl: imgUrl
            });
        }
    }


    render() {
        const cameraBtn = this.state.streaming ? 
                        <div onClick={() => this.handleClick()} className="camera-btn btn"></div> : 
                        null;
        return (
            <div className="camera">
                <div className="video-block">
                    <video id="video" autoPlay="true" onCanPlay={(e) => this.handleCanPlay()} />
                    <canvas id="canvas"></canvas>
                    { cameraBtn }
                </div>
                <Photo imgUrl={this.state.imgUrl} width={this.state.width} height={this.state.height} />
            </div>
        )
    }
}

export default connect((state) => ({ state: state }), actions)(Camera);