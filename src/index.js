import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducer from './reducer';

import './index.css';
import WaterColor from './WaterColor';
import registerServiceWorker from './registerServiceWorker';

const store = createStore(reducer);

ReactDOM.render(
    <Provider store={store}>
        <WaterColor />
    </Provider>, document.getElementById('root'));
registerServiceWorker();
