import React, { Component } from 'react';
import './WaterColor.css';
import { connect } from 'react-redux';
import * as actions from './actions';

import Camera from './components/Camera';

import { MAX_MOBILE_WIDTH, MOBILE_CAMERA_WIDTH, DESKTOP_CAMERA_WIDTH } from './components/constant';
import img1 from './images/img1.png';
import img2 from './images/img2.png';
import img3 from './images/img3.png';

class WaterColor extends Component {
	render() {
		const width = window.innerWidth;
		const cameraWidth =  width < MAX_MOBILE_WIDTH ? MOBILE_CAMERA_WIDTH: DESKTOP_CAMERA_WIDTH;
		return (
			<div className="container">
				<header>
					<h1>WaterColor World</h1>
				</header>
				<Camera cameraWidth={cameraWidth}/>
				<div className="examples">
					<h2>Примеры фотографий из акварельного мира</h2>
					<div className="photos">
						<img src={img1} alt='img1' />
						<img src={img2} alt='img2' />
						<img src={img3} alt='img3' />
					</div>
				</div>
			</div>
		);
	}
}

export default connect((state) => ({state: state}), actions)(WaterColor);
